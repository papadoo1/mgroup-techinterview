angular.module('mgroup-techinterview', ['ui.router', 'ngResource', 'mgroup-techinterview.controllers', 'mgroup-techinterview.services']);

angular.module('mgroup-techinterview').config(function($stateProvider) {
  // state for showing eqs and comps
  $stateProvider.state('overview', { 
    url: '/overview',
    templateUrl: 'partials/overview.html',
    controller: 'OverviewListController'
  }).state('viewEQ', { //state for showing single eq
    url: '/eqs/:id/view',
    templateUrl: 'partials/EQ-view.html',
    controller: 'EQPresetViewController'
  }).state('viewComp', { //state for showing single compressor
    url: '/comps/:id/view',
    templateUrl: 'partials/comp-view.html',
    controller: 'CompPresetViewController'
  }).state('newEQ', { //state for adding a new eq
    url: '/eqs/new',
    templateUrl: 'partials/eq-add.html',
    controller: 'EQCreateController'
  }).state('newComp', { //state for adding a new compressor
    url: '/comps/new',
    templateUrl: 'partials/comp-add.html',
    controller: 'CompCreateController'
  }).state('editEQ', { //state for updating an eq
    url: '/eqs/:id/edit',
    templateUrl: 'partials/eq-edit.html',
    controller: 'EQEditController'
  }).state('editComp', { //state for updating a compressor
    url: '/comps/:id/edit',
    templateUrl: 'partials/comp-edit.html',
    controller: 'CompEditController'
  });
}).run(function($state) {
  $state.go('overview'); //make a transition to overview state when the app begins
});