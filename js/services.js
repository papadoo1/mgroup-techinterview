angular.module('mgroup-techinterview.services', []).factory('EQ', function($resource) {
  return $resource('/api/eq/:eq_id', { eq_id: '@_id' }, {
    update: {
      method: 'PUT'
    }
  });
}).
factory('Compressor', function($resource) {
  return $resource('/api/comp/:comp_id', { comp_id: '@_id' }, {
    update: {
      method: 'PUT'
    }
  });
});