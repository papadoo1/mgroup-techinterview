angular.module('mgroup-techinterview.controllers', [])
    
.controller('OverviewListController', function($scope, $state, $window, EQ, Compressor) {
  // fetch all eqs and all comps
  $scope.eqs = EQ.query();
  $scope.comps = Compressor.query();
  
  $scope.deleteEQ = function(eq) { 
      eq.$delete(function() {
        $window.location.href = ''; //redirect to home
      });
  };
  
  $scope.deleteComp = function(comp) { 
      comp.$delete(function() {
        $window.location.href = ''; //redirect to home
      });
  };
})

.controller('EQPresetViewController', function($scope, $stateParams, EQ) {
  //Get a single eq preset. Issues a GET to     /api/eq/:id
  $scope.eq = EQ.get({ eq_id: $stateParams.id }); 
})

.controller('CompPresetViewController', function($scope, $stateParams, Compressor) {
  //Get a single compressor preset. Issues a GET to /api/comp/:id
  $scope.comp = Compressor.get({ comp_id: $stateParams.id }); 
})

.controller('EQCreateController', function($scope, $state, $stateParams, EQ) {
    
  $scope.eq = new EQ();  //create new EQ instance. Properties will be set via ng-model on UI

  $scope.addEQ = function() { 
    //create a new eq. Issues a POST to /api/eq
    $scope.eq.$save(function() {
      $state.go('overview');// on success go back
    });
  };
})

.controller('CompCreateController', function($scope, $state, $stateParams, Compressor) {
  
  // Create new compressor instance. 
  // Properties will be set via ng-model on UI
  $scope.comp = new Compressor();  

  $scope.addComp = function() { 
    //create a new comp. Issues a POST to /api/comp
    $scope.comp.$save(function() {
      $state.go('overview');// on success go back
    });
  };
})

.controller('EQEditController', function($scope, $state, $stateParams, EQ) {
  $scope.updateEQ = function() {
    $scope.eq.$update(function() {
      // on success go back to home i.e. overview state.
      $state.go('overview'); 
    });
  };

  $scope.loadEQ = function() { 
    //Issues a GET request to /api/eq/:id 
    $scope.eq = EQ.get({ eq_id: $stateParams.id });
  };

  $scope.loadEQ(); // Load an EQ which can be edited on UI
})
.controller('CompEditController', function($scope, $state, $stateParams, Compressor) {
  
  $scope.updateComp = function() {
    $scope.comp.$update(function() {
      // on success go back to home i.e. overview state.
      $state.go('overview'); 
    });
  };

  $scope.loadComp = function() { 
    //Issues a GET request to /api/comp/:id 
    $scope.comp = Compressor.get({ comp_id: $stateParams.id });
  };

  $scope.loadComp();
});