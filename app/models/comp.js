var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

// Enumerator for compressor modes
// I assumed there will be some usual compressor types

var CompSchema  = new Schema({
    preset_name: String,
    attack: Number,
    release: Number,
    threshold: Number,
    ratio: String,  // I got a bit lazy here, there should be
                    // better validation for the ratio.
    presence: Number,
    makeup: Number,
    mode: String
});

module.exports = mongoose.model('Comp', CompSchema);