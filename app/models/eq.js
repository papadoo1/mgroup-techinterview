var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

// enumerator for validating shelf states
var shelves = 'peak shelf Peak Shelf'.split(' ');
// enumerator for q
var qenum = 'high low High Low hi lo Hi Lo'.split(' ')
// enumerator for on off
var onoffenum = 'on off On Off'.split(' ')

var EqSchema  = new Schema({
    
    preset_name: String,
    
    // band switches
    low_band_switch: {
        type: String,
        enum: onoffenum
    },
        
    lowmid_band_switch: {
        type: String,
        enum: onoffenum
    },
    himid_band_switch: {
        type: String,
        enum: onoffenum
    },
    hi_band_switch: {
        type: String,
        enum: onoffenum
    },
    
    // shelves and q
    low_peak_or_shelf: {
        type: String,
        enum: shelves
    },
    hi_peak_or_shelf:{
        type: String,
        enum: shelves
    },
    lowmid_high_or_low_q :{
        type: String,
        enum: qenum
    },
    
    // freq
    low_freq: Number,
    lowmid_freq: Number,
    himid_freq: Number,
    hi_freq: Number,
   
    // gain
    low_gain: Number,
    lowmid_gain: Number,
    himid_gain: Number,
    hi_gain: Number
    
});

module.exports = mongoose.model('Eq', EqSchema);