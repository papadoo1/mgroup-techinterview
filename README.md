#Music Group Technical Task
## Overview
This application was developed as part of a technical task in the interview process for Music Group.

The tasks were: 

* Produce a RESTful API service for creating, reading, updating and deleting some user presets
* Demonstrate the API with a basic web GUI client
* Provide your task in a git repository

The RESTFul API was written using __Node.js__ retreiving data from a __Mongodb__ instance (in my [MongoLab](https://mongolab.com) sandbox). Some other libraries/frameworks that were used were __Express 4__, __Mongoose__ and __Body-Parser__.

In order to develop a GUI for the demonstration of the results I used __Angular.js__ with __angular-resource__ and __angular-ui-router__

## Installation notes

* Clone the repo  
* Make sure you have Node installed  
* From the root directory run ```npm install```  
* From the root directory run ```node server.js```
* Your server now lives at [localhost:8080](http://localhost:8080/)

## API

| URL        | Method           | Result  |
| ------------- |:-------------:| -----|
| /api/eq      | GET | returns all the eq presets |
| /api/eq     | POST      |   creates a new eq |
| /api/eq/:id | GET      |    returns the eq with the specific id |
| /api/eq/:id | PUT      |    updates the eq with the specific id |
| /api/eq/:id | DELETE      |    deletes the eq with the specific id |

The same applies for */api/comp   

The API was tested using [Postman](https://www.getpostman.com).

## Comments & Assumptions
I didn't add server validation in the values. Only enumerators so some of the switches(on/off, peak/shelf) make sense, instead of being booleans

I also assumed that there is one type of EQ and one type of Compressor and their attributes are the same.

The data modelling and the names I chose are not ideal. EQ should have been EQ Preset same with Compressor, but for the sake of simplicity I left them like this.

Also the messages are not ideal, there is no sophisticated logging or any useful error messages -- except the ones provided by the frameworks that I used.

I might have over-complicated it with Angular. But when you asked me about how will you go about displaying other pages I didn't know about Angular-UI-router so I spend some time and learned about it. This unlocked a whole new world of possibilities with Angular for me.

Finally, the front-end is just me messing about. It could have been way simpler but I decided to kill some time with it.