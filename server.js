//=====================================================
// CONFIG
//=====================================================

var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');

// DB Setup
var options = { server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }, 
                replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } } }; 

// -- this mongo db instance lives in the mongoLab cloud
var mongodbUri = 'mongodb://node:node@ds023458.mlab.com:23458/mgroup-techinterview-db';

mongoose.connect(mongodbUri, options);

// Check db connection
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    // Success!
});

// Load the db models
var Eq = require('./app/models/eq');
var Comp = require('./app/models/comp');

// Configure express app to use bodyParser()
// -- this will allow POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// set the listening port
var port = process.env.PORT || 8080;

//=====================================================
// API ROUTES
//=====================================================

var router = express.Router();

// Middleware to use for all requests.
// Logging happens here.
// We can add validations here to make sure that everything
// that comes and goes is correct.
// Or even make sure that the user is authenticated. 
// The sky is the limit... 
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next();
});

// test route to make sure everything is working
router.get('/', function(req, res) {
    res.json({ message: 'hello darkness my old friend' });   
});

// more routes for our API will happen here

// routes that end in /eq
router.route('/eq')
    // create an eq (accessed at POST /api/eq)
    .post(function(req, res) {
    
        Eq.create(req.body, function (err, post) {
            if (err) res.json ({message: err});
                res.json({ message: 'Eq created!' });
        });
//        var eq = new Eq();
//        eq.preset_name = req.body.preset_name;
//    
//        eq.low_band_switch = req.body.low_band_switch;
//        eq.lowmid_band_switch = req.body.lowmid_band_switch;
//        eq.himid_band_switch = req.body.himid_band_switch;
//        eq.hi_band_switch = req.body.hi_band_switch;
//    
//        eq.low_peak_or_shelf = req.body.low_peak_or_shelf;
//        eq.lowmid_high_or_low_q = req.body.lowmid_high_or_low_q;
//        eq.hi_peak_or_shelf = req.body.hi_peak_or_shelf;
//    
//        eq.low_freq = req.body.low_freq;
//        eq.lowmid_freq = req.body.lowmid_freq;
//        eq.himid_freq = req.body.himid_freq;
//        eq.hi_freq = req.body.hi_freq;
//    
//        eq.low_gain = req.body.low_gain;
//        eq.lowmid_gain = req.body.lowmid_gain;
//        eq.himid_gain = req.body.himid_gain;
//        eq.hi_gain = req.body.hi_gain;
//        
//        // save and check for errors
//        eq.save(function(err) {
//            if (err)
//                res.send(err);
//
//            res.json({ message: 'Eq created!' });
//        });

    })
    
    // get all the eqs
    .get(function(req, res) {
        Eq.find(function(err, eqs) {
            if (err)
                res.send(err);

            res.json(eqs);
        });
    });

// routes that end in /eq/:eq_id
router.route('/eq/:eq_id')

    // get the eq with id=eq_id 
    .get(function(req, res) {
        Eq.findById(req.params.eq_id, function(err, eq) {
            if (err)
                res.send(err);
            res.json(eq);
        });
    })
    // update the eq info
    .put(function(req, res) {
        Eq.findByIdAndUpdate(req.params.eq_id, req.body, 
            function(err, eq) {
            
                if (err) return next(err);
                    res.json("Updated!");
            
            }
        );
    })
    // delete the eq
    .delete(function(req, res) {
        Eq.remove({
            _id: req.params.eq_id
        }, function(err, eq) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

// routes that end in /comp
router.route('/comp')
    .post(function(req,res){
        Comp.create(req.body, function (err, post) {
            if (err) {console.log(err); return res.json(err);}
                res.json({ message: 'Compressor created!' });
        });
    })
     // get all the compressor presets
    .get(function(req, res) {
        Comp.find(function(err, comps) {
            if (err)
                res.send(err);
            res.json(comps);
        });
    });

// routes that end in /comp/:comp_id
router.route('/comp/:comp_id')

    // get the compressor with id=comp_id 
    .get(function(req, res) {
        Comp.findById(req.params.comp_id, function(err, comp) {
            if (err)
                res.send(err);
            res.json(comp);
        });
    })
    // update the compressor info
    .put(function(req, res) {
        Comp.findByIdAndUpdate(req.params.comp_id, req.body, 
            function(err, eq) {  
                if (err) return res.json(err);
                    res.json("Updated!");  
            }
        );
    })
    // delete the compressor
    .delete(function(req, res) {
        Comp.remove({
            _id: req.params.comp_id
        }, function(err, comp) {
            if (err)
                res.send(err);
            
            res.json({ message: 'Successfully deleted' });
        });
    });


// REGISTER OUR ROUTES -------------------------------
// all of the routes will be prefixed with /api
app.use('/api', router);

// Serves index
app.get('/', function (req, res) {
  res.sendfile('index.html');
});

// sends all the static files
app.get(/^(.+)$/, function(req, res){ 
     console.log('static file request : ' + req.params);
     res.sendfile( __dirname + req.params[0]); 
 });

//=====================================================
// SERVER STARTUP
//=====================================================

app.listen(port);
console.log('Magic happens on port ' + port);